package ru.t1.dsinetsky.tm.model;

import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

public class Command {

    private String name = "";

    private String argument;

    private String desc = "";

    public Command(String name, String argument, String desc) {
        this.name = name;
        this.argument = argument;
        this.desc = desc;
    }

    public Command() {

    }

    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION,
            ArgumentConst.CMD_VERSION,
            "Shows program version"
    );

    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT,
            ArgumentConst.CMD_ABOUT,
            "Shows information about developer"

    );

    public static Command HELP = new Command(
            TerminalConst.CMD_HELP,
            ArgumentConst.CMD_HELP,
            "Shows this message"
    );

    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT,
            null,
            "Exit application"
    );

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        String result = "";

        if (argument != null && !argument.isEmpty()) result += argument + ",";
        if (name != null && !name.isEmpty()) result += name + " - ";
        if (desc != null && !desc.isEmpty()) result += desc;
        return result;
    }
}
