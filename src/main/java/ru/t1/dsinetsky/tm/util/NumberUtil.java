package ru.t1.dsinetsky.tm.util;

public interface NumberUtil {

    static String formatBytes(long bytes){
        long kilobyte = 1024;
        long megabyte = 1024 * kilobyte;
        long gigabyte = 1024 * megabyte;
        long terabyte = 1024 * gigabyte;
        if ((bytes >= 0) && (bytes < kilobyte)){ return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)){ return bytes / kilobyte + " Kb";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)){ return bytes / megabyte + " Mb";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)){ return bytes / gigabyte + " Gb";
        } else {
            return bytes / terabyte + " Tb";
        }
    }

}
